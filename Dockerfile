ARG RUBY_PATH=/usr/local/

#### ******** ####
#### BUILDERS ####
#### ******** ####

## BUILD: RUBY ##
# this is mostly to avoid putting build-essential
# on the final image, which I may end up needing
# anyway for various rubygems
FROM ubuntu:focal as ruby-builder
LABEL builder=true
ARG RUBY_VERSION="3.0.1"
ARG RUBY_PATH
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update -y
RUN apt-get install -y --no-install-recommends apt-utils
RUN apt-get install -y --no-install-recommends \
 ca-certificates \
 curl \
 wget \
 git \
 build-essential
RUN apt-get install -y --no-install-recommends zlib1g-dev \
 libssl-dev \
 libreadline-dev \
 libyaml-dev \
 libxml2-dev \
 libxslt-dev \
 libffi-dev
RUN git clone https://github.com/rbenv/ruby-build.git $RUBY_PATH/plugins/ruby-build \
&& $RUBY_PATH/plugins/ruby-build/install.sh
RUN ruby-build $RUBY_VERSION $RUBY_PATH
RUN echo 'gem: --no-document' > /usr/local/etc/gemrc
RUN gem update --system
RUN gem update bundler --force
ADD Gemfile /root/Gemfile
WORKDIR /root
RUN bundle install

## BUILD: RUST TOOLS ##
FROM rust:1.52.1-alpine3.13 as rust-tools-builder
LABEL builder=true
RUN apk --update add git less openssh musl-dev make
RUN cargo install ag

#### ******** ####
####   CORE   ####
#### ******** ####
FROM ubuntu:focal
LABEL maintainer="jyoung@gitlab.com"
ENV DEBIAN_FRONTEND=noninteractive
ENV TERM="xterm-256color"
FROM ubuntu:focal
LABEL builder=true
LABEL maintainer="jyoung@gitlab.com"
### UPDATES ###
ENV DEBIAN_FRONTEND=noninteractive
ENV LANG C.UTF-8
RUN apt-get update -y && apt-get upgrade -y

### BASETOOLS ###
# build-essential is huge and bloats the image, but building anything -
# and future flexibility - requires it, so ¯\_(ツ)_/¯
RUN yes | unminimize
RUN apt-get install -y --no-install-recommends apt-utils
RUN apt-get install -y --no-install-recommends \
 ca-certificates \
 curl \
 wget \
 git \
 man \
 manpages-posix \
 locales
### COREUTILS ###
# ubuntu:focal base already includes gzip, tar, sed,
# add a set of distribution utilities as individual layers
RUN apt-get install -y --no-install-recommends vim
RUN apt-get install -y --no-install-recommends openssl
RUN apt-get install -y --no-install-recommends jq
RUN apt-get install -y --no-install-recommends jid
RUN apt-get install -y --no-install-recommends gawk
RUN apt-get install -y --no-install-recommends less
RUN apt-get install -y --no-install-recommends bash-completion
RUN apt-get install -y --no-install-recommends fzf
RUN apt-get install -y --no-install-recommends tmux
RUN apt-get install -y --no-install-recommends unzip
RUN apt-get install -y --no-install-recommends mc
RUN apt-get install -y --no-install-recommends strace
RUN apt-get install -y --no-install-recommends lsof
RUN apt-get install -y --no-install-recommends silversearcher-ag
#  https://github.com/sharkdp/bat/issues/938
RUN apt-get install -y --no-install-recommends  -o Dpkg::Options::="--force-overwrite" bat ripgrep
RUN ln -s /usr/bin/batcat /usr/bin/bat
# TODO some of the network utilities would be useful too
# e.g. part of the list of: https://github.com/nicolaka/netshoot

### RUBY ###
ARG RUBY_PATH
# packages needed for rubygems
# I'm not completely sure what all is actually needed here.
# and not all of these may be needed
RUN apt-get install -y --no-install-recommends libffi-dev
RUN apt-get install -y --no-install-recommends libyaml-dev
# RUN apt-get install -y --no-install-recommends zlib1g-dev
# RUN apt-get install -y --no-install-recommends libssl-dev
# RUN apt-get install -y --no-install-recommends libreadline-dev
# RUN apt-get install -y --no-install-recommends libxml2-dev
# RUN apt-get install -y --no-install-recommends libxslt-dev
COPY --from=ruby-builder $RUBY_PATH $RUBY_PATH

### RUST TOOLS ###
COPY --from=rust-tools-builder /usr/local/cargo/bin/agrind /usr/local/bin/agrind

### SUPPORT UTILITIES ###
RUN wget -c https://gitlab.com/gitlab-com/support/toolbox/strace-parser/uploads/aeec995aa6d28ce0ddc5e8dab3952278/strace-parser_0-7-2_linux_musl.tar.gz -O - | tar -xz -C /usr/local/bin
RUN wget -c https://gitlab.com/gitlab-com/support/toolbox/fast-stats/uploads/03d2c7d0c5180b4313f5e868dcb6426f/fast-stats_0-7-0_linux-musl.tar.gz -O - | tar -xz -C /usr/local/bin

### SHELL CUSTOMIZATION ###
# bash customizations
ADD .bash_customize /root/.bash_customize
RUN echo "source /root/.bash_customize" >> /root/.bashrc

### CLEANUP ###
RUN rm -Rf /var/lib/apt/lists/* && apt-get clean
