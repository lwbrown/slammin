# Support Log Analysis Manager

It's _slammin_!
# Included Tools

* Archive Tools
  * `gzip` : https://www.gzip.org/
  * `tar`
  * `unzip`

* Editors
  * `vim` : https://www.vim.org/

* Pagers
  * `less` : http://www.greenwoodsoftware.com/less/
  * `bat` : https://github.com/sharkdp/bat

* Text filtering utilities
  * `grep` ( included with `ubuntu:focal`)
  * `awk` - actually `mawk` ( included with `ubuntu:focal` )
  * `sed` ( included with `ubuntu:focal` )
  * `gawk` : https://www.gnu.org/software/gawk/manual/gawk.html
  * `fzf` : https://github.com/junegunn/fzf
  * `jid` : https://github.com/simeji/jid
  * `ripgrep` : https://github.com/BurntSushi/ripgrep
  * `jq` : https://stedolan.github.io/jq/
  * `ag` (silver searcher) : https://github.com/ggreer/the_silver_searcher
  * `agrind`: https://github.com/rcoh/angle-grinder (**NOTE**  - this may be the one utility to rule them all!)

* Misc:
  * `mc` (Midnight commander) : https://midnight-commander.org/
  * `strace` ( https://strace.io/ ) and `lsof` are included to help with comparing (can be used against container processes to observe typical operations)
  * `tmux` : https://github.com/tmux/tmux/wiki
  * `openssl` : https://www.openssl.org/
  * `curl` : https://curl.se/
  * `wget` : https://www.gnu.org/software/wget/
  * `git` : https://git-scm.com/

* GitLab Support:
  * `strace-parse`: https://gitlab.com/gitlab-com/support/toolbox/strace-parser/
  * `fast-stats` : https://gitlab.com/gitlab-com/support/toolbox/fast-stats/

* Scripting
  * `bash` : https://www.gnu.org/software/bash/
  * `ruby` : https://ruby-doc.org/

* RubyGems
  * `labclient` : https://labclient.gitlab.io/labclient/
  * `down` : https://github.com/janko/down
  * `pry` : https://pry.github.io/
# Invocation

```sh
TICKET_DIR=(basename $PWD) \
 docker run --interactive --tty \
 --volume $PWD:/$TICKET_DIR \
 --workdir /$TICKET_DIR \
 --env TICKET=$TICKET_DIR \
 registry.gitlab.com/jayo/slammin/main:latest \
  /bin/bash
```

Optionally - mount a `slammin-tools` volume at `/slammin-tools` for tools/customizations.

```sh
TICKET_DIR=(basename $PWD) \
 docker run --interactive --tty \
 --volume $PWD:/$TICKET_DIR \
 --volume $HOME/dev/slammin-tools:/slammin-tools  \
 --workdir /$TICKET_DIR \
 --env TICKET=$TICKET_DIR \
 registry.gitlab.com/jayo/slammin/main:latest \
  /bin/bash
```

Keep a per-ticket history file by mounting that into the container as well:

```sh
touch $PWD/slammin_history.log && \
TICKET_DIR=(basename $PWD) \
 docker run --interactive --tty \
 --volume $PWD:/$TICKET_DIR \
 --mount type=bind,source=$PWD/slammin_history.log,target=/root/.bash_history \
 --volume $HOME/dev/slammin-tools:/slammin-tools  \
 --workdir /$TICKET_DIR \
 --env TICKET=$TICKET_DIR \
 registry.gitlab.com/jayo/slammin/main:latest \
 /bin/bash
```

(using `--mount` will ensure that the file exists before mounting it in the container)
# Customization

If present, `/slammin-tools/bash_customizations` will be sourced via a `.bash_customize` that's included on the Docker image (which is in turn, sourced from root's .bashrc: `RUN echo "source /root/.bash_customize" >> /root/.bashrc`)

`.bash_customize` contents:

```bash
# tools customization
if [ -f /slammin-tools/bash_customizations ]; then
    echo "Sourcing /slammin-tools/bash_customizations"
    . /slammin-tools/bash_customizations
fi
```

The usual docker volumes tricks to replace `.bashrc` or other dotfile customizations can be done as well.

Or build your own!  You can source `registry.gitlab.com/jayo/slammin/main:latest` in a `FROM` directive in your own Dockerfile

See [slammin-tools](https://gitlab.com/jayo/slammin-tools) for customization and add-on tools examples
